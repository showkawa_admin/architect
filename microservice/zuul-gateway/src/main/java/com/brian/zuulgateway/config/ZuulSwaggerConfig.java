package com.brian.zuulgateway.config;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-06-23 12
 **/
@Component
@Primary
public class ZuulSwaggerConfig implements SwaggerResourcesProvider {


    @Override
    public List<SwaggerResource> get() {
        List resources = new ArrayList<>();
        resources.add(swaggerResource("brian-order-server", "/api-order-dev/v2/api-docs", "2.0"));
        resources.add(swaggerResource("brian-member-impl-server", "/api-order/v2/api-docs", "2.0"));
        resources.add(swaggerResource("brian-order-impl-server", "/api-order/v2/api-docs", "2.0"));
        return resources;

    }

    private SwaggerResource swaggerResource(String name, String location, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }
}

