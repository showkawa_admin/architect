package com.brian.apolloclient.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: microservice
 * @author: Brian Huang
 * @create: 2019-06-20 23
 **/
@RestController
public class ApolloController {

    @Value("${test_dev_attr}")
    public String value;


    @GetMapping("/getProperty")
    public String getDevProperty(){
        return value;
    }

}
