package com.brian.apimemberservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiMemberServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiMemberServiceApplication.class, args);
    }

}
