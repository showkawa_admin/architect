package com.brian.apimemberservice.service;

import org.springframework.web.bind.annotation.GetMapping;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-06-14 18
 **/
public interface MemberService {

    @GetMapping("/getMember")
    public String getMember(String name);

    @GetMapping("/getString")
    public String getString();
}
