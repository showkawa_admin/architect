package com.brian.apiorderserviceimpl.service.impl;

import com.brian.apiorderservice.service.OrderService;
import com.brian.apiorderserviceimpl.feign.OrderServiceFeign;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-06-14 11
 **/
@RestController
@Api("订单服务接口")
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderServiceFeign orderServiceFeign;

    @GetMapping("/getMember")
    @Override
    @ApiOperation("获取会员信息")
    @ApiImplicitParam(name = "name",value = "用户名称",required = true,dataTypeClass = String.class)
    public String order2Member(@RequestParam("name") String name) {
        return orderServiceFeign.order2Member(name);
    }

    @Override
    @GetMapping("/getString")
    @ApiOperation("断路器基于类实现的测试")
    public String test() {
        System.out.println("当前线程；"+ Thread.currentThread().getName());
        return orderServiceFeign.test();
    }

    @HystrixCommand(fallbackMethod="callbackH")
    @GetMapping("/getStringH")
    @ApiOperation("断路器基于注解实现的测试")
    public String testH() {
        System.out.println("H当前线程；"+ Thread.currentThread().getName());
        return orderServiceFeign.test();
    }

    @GetMapping("/callbackH")
    public String callbackH(){
        return "服务忙稍后再试。。。。";
    }


}
