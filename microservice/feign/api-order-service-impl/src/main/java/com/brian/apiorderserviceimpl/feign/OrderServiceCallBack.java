package com.brian.apiorderserviceimpl.feign;

import org.springframework.stereotype.Component;

/**
 * @program: microservice
 * @author: Brian Huang
 * @create: 2019-06-22 12
 **/
@Component
public class OrderServiceCallBack implements OrderServiceFeign {

    @Override
    public String order2Member(String name) {
        return "Order2Member 接口繁忙，请稍后在试！！！";
    }

    @Override
    public String test() {
        return "Test 接口繁忙，请稍后在试！！！";
    }
}
