package com.brian.singleton.singleton;

import java.io.Serializable;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-30 20
 *
 * 饿汉式单例  就初始化一次
 * 优点；先天线程安全 => 当类被加载是就创建了实例
 * 缺点：static修饰的占用方法区，定义太多即使不使用也会占用内存,项目启动也会变慢
 **/
public class Singleton饿汉式1 implements Serializable {

    private static Singleton饿汉式1 singleton饿汉式 = new Singleton饿汉式1();

    //无参构造函数 private
    private Singleton饿汉式1() {
    }

    public static Singleton饿汉式1 getSingleton饿汉式() {
        return singleton饿汉式;
    }

    public static void main(String[] args) {
        Singleton饿汉式1 singleton饿汉式1 = Singleton饿汉式1.getSingleton饿汉式();
        Singleton饿汉式1 singleton饿汉式2 = Singleton饿汉式1.getSingleton饿汉式();
        System.out.println(singleton饿汉式1 == singleton饿汉式2);
    }


    //返回序列化获取对象 ，保证为单例，防止序列化破解
    public Object readResolve() {
        return singleton饿汉式;
    }

}
