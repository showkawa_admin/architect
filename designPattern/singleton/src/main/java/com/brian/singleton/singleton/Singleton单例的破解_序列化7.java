package com.brian.singleton.singleton;

import java.io.*;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-30 21
 **/
public class Singleton单例的破解_序列化7 {

    //序列化  从内存写入到本地磁盘
    //反序列化 从本地磁盘写到内存中
    public static void main(String[] args) throws Exception {
        //序列化
        Singleton饿汉式1 instance = Singleton饿汉式1.getSingleton饿汉式();
        FileOutputStream fos = new FileOutputStream(System.getProperty("user.dir")+"\\Singleton.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(instance);
        oos.flush();
        oos.close();

        //反序列化
        FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\Singleton.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);
        Singleton饿汉式1 instance2 = (Singleton饿汉式1) ois.readObject();
        System.out.println(instance2==instance);
    }
    //怎么防止序列化破解
    //重写readResolve()方法返回当前实例

}
