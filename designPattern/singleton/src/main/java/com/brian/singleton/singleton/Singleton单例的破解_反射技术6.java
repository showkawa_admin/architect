package com.brian.singleton.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-30 21
 * 单例的基本原则： 在单个JVM中只有一个实例
 *
 * 虽然都是私有的 但是通过java反射技术或者序列化，也可以去初始化
 **/
public class Singleton单例的破解_反射技术6 {

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Singleton懒汉式2 懒汉式 = Singleton懒汉式2.getSingleton懒汉式();

        Class<?> aClass = Class.forName("com.brian.singleton.singleton.Singleton懒汉式2");
        Constructor<?> declaredConstructor = aClass.getDeclaredConstructor();
        declaredConstructor.setAccessible(true);
        Singleton懒汉式2 懒汉式2 = (Singleton懒汉式2) declaredConstructor.newInstance();
        System.out.println(懒汉式 == 懒汉式2);

    }
    //1.如何防御java反射技术破解单例模式
    //因为此处式通过构造函数来实例化，所以可以在构造函数里面判断是否已经实例化，如果已经实例化就抛出异常

    //1可以阻止java反射技术破解单例，但是如果调换 懒汉式 和 懒汉式2的创建顺序还是不能保证不被破解






}
