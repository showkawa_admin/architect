package com.brian.singleton.singleton;

/**
 * @program: base
 * @author: Brian Huang
 * @create: 2020-07-11 21
 *
 *  Singleton静态代码块4 初始化的时候就会加载，属于饿汉式
 **/
public class Singleton静态代码块4 {

    private static Singleton静态代码块4 singleton静态代码块4;

    private Singleton静态代码块4 () {

    }

    static {
        singleton静态代码块4 = new Singleton静态代码块4();
    }

    public static Singleton静态代码块4 getSingleton静态代码块4() {
        return singleton静态代码块4;
    }

    public static void main(String[] args) {
        for(int i=0;i<20;i++) {
            new Thread(() -> System.out.println(Thread.currentThread().getName() + " " +Singleton静态代码块4.getSingleton静态代码块4().toString())).start();
        }

    }
}
