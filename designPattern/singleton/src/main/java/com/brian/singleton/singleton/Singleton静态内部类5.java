package com.brian.singleton.singleton;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-30 21
 *
 *  继承了懒汉式和饿汉式的优点
 *          内部类在被调用的时候才会初始化对象
 **/
public class Singleton静态内部类5 {

    private Singleton静态内部类5() {
        System.out.println("构造函数初始化。。。。");
    }

    public static class  SingletonUtil{
        private  static Singleton静态内部类5 singleton静态内部类 = new Singleton静态内部类5();
    }


    public static void main(String[] args) {
        System.out.println("main方法启动。。。。");
        Singleton静态内部类5 singleton静态内部类1 = SingletonUtil.singleton静态内部类;
        Singleton静态内部类5 singleton静态内部类2 = SingletonUtil.singleton静态内部类;
        System.out.println(singleton静态内部类1 == singleton静态内部类2);
    }
}
