package com.brian.template;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class AliPaymentTemplate extends PaymentTemplate {
    @Override
    public String updateOrder() {
        if(true){
            return returnSuccess();
        }
        return returnFailed();
    }

    @Override
    public String verifySignature() {
        return "支付宝支付验签";
    }

    @Override
    public String returnSuccess() {
        return "支付宝支付成功 200";
    }

    @Override
    public String returnFailed() {
        return "支付宝支付失败 201";
    }
}
