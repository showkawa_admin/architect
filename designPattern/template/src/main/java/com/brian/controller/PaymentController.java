package com.brian.controller;


import com.brian.service.TemplateFactory;
import com.brian.template.PaymentTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentController {

    @Autowired
    private TemplateFactory templateFactory;

    @GetMapping("/asyncCallbackPay")
    public ResponseEntity<String> asyncCallbackPay(@RequestParam String payType){
        PaymentTemplate paymentTemplate = templateFactory.getPaymentTemplate(payType);
        String result = paymentTemplate.asyncPay();
        return new ResponseEntity<>(result, HttpStatus.OK);

    }

    @GetMapping("/")
    public ResponseEntity<String> test(){
        return new ResponseEntity<String>("8083",HttpStatus.OK);
    }


}
