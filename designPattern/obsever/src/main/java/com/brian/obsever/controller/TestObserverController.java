package com.brian.obsever.controller;

import com.brian.obsever.observer.subject.impl.AlertSubject;
import com.brian.obsever.observer_by_spring.event.OrderMessageEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-24 09
 **/
@RestController
public class TestObserverController {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private AlertSubject alertSubject;


    /**
     *  模拟运维平台 通知运维人员
     * @param message
     * @return
     */
    @GetMapping("/testAlertMessage")
    public ResponseEntity<?> order(@RequestParam String message){
        alertSubject.notifyObserver(message);
        return new ResponseEntity<>("消息已经发出", HttpStatus.OK);

    }


    /**
     *  模拟用户订票通知用户
     * @return
     */
    @GetMapping("/order")
    public ResponseEntity<?> order(){
        Map<String, String> map = new HashMap<>();
        map.put("orderId","QW12345676545");
        map.put("content","2020-07-01 慕尼黑 -> 香港 KA800 航班");
        map.put("price","$1000");
        OrderMessageEvent orderMessageEvent = new OrderMessageEvent(this,map);
        applicationContext.publishEvent(orderMessageEvent);
        return new ResponseEntity<>(map, HttpStatus.OK);

    }
}
