package com.brian.obsever.observer_by_spring.event;

import org.springframework.context.ApplicationEvent;

import java.util.Map;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-24 09
 **/
public class OrderMessageEvent extends ApplicationEvent {

    //群发消息的内容
    private Map map;

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public OrderMessageEvent(Object source, Map map) {
        super(source);
        this.map = map;
    }
}
