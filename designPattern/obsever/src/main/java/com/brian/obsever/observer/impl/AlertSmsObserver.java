package com.brian.obsever.observer.impl;

import com.brian.obsever.observer.Observer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @program: base
 * @author: Brian Huang
 * @create: 2020-07-11 17
 **/
@Component
@Slf4j
public class AlertSmsObserver implements Observer {

    /**
     *  短信通知运维
     * @param message
     */
    @Override
    public void sendMessage(String message) {
        log.info("短信通知运维人员: {}",message);
    }
}
