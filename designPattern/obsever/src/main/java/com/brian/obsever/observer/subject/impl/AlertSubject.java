package com.brian.obsever.observer.subject.impl;

import com.brian.obsever.observer.subject.BrianSubject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.brian.obsever.observer.Observer;
import org.springframework.stereotype.Component;

/**
 * @program: architect
 * @author: Brian Huang  具体的主题
 * @create: 2019-05-22 22
 **/
@Component
public class AlertSubject extends BrianSubject {

    private List<Observer> observerList ;

    private ExecutorService executorService;

    public AlertSubject() {
        this.observerList = new ArrayList<>();
        this.executorService = Executors.newFixedThreadPool(10);
    }

    @Override
    public void addObserver(Observer observer) {
        observerList.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observerList.remove(observer);
    }

    @Override
    public void notifyObserver(String message) {
        observerList.forEach(observer -> {
            executorService.execute(() -> observer.sendMessage(message));
        });
    }
}
