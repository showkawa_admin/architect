package com.brian.obsever.observer;

/**
 * @program: architect
 * @author: Brian Huang 抽象观察者
 * @create: 2019-05-22 22
 **/
public interface Observer {
    /**
     *通知观察者消息
     * @param message
     */
    void sendMessage(String message);
}
