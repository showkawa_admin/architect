package com.brian.obsever.observer.subject;


import com.brian.obsever.observer.Observer;

/**
 * @program: architect 抽象的主题
 * @author: Brian Huang
 * @create: 2019-05-22 22
 **/
public abstract class BrianSubject {
    /**
     * 添加观察者
     * @param observer
     */
   public abstract void addObserver(Observer observer);

    /**
     * 移除观察者
     * @param observer
     */
    public abstract void removeObserver(Observer observer);

    /**
     * 通知消息
     * @param message
     */
    public abstract void notifyObserver(String message);
}
