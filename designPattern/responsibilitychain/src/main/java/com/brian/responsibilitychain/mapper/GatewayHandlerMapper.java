package com.brian.responsibilitychain.mapper;

import com.brian.responsibilitychain.entity.GatewayHandlerEntity;
import org.apache.ibatis.annotations.Select;

public interface GatewayHandlerMapper {


    //获取第一个GatewayHandler
    @Select("SELECT  handler_name AS handlerName,handler_id AS handlerid ,prev_handler_id AS prevhandlerid ,next_handler_id AS nexthandlerid  FROM GATEWAY_HANDLER WHERE  prev_handler_id is null;")
    public GatewayHandlerEntity getFirstGatewayHandler();

    @Select("SELECT  handler_name AS handlerName,handler_id AS handlerid ,prev_handler_id AS prevhandlerid ,next_handler_id AS nexthandlerid   FROM GATEWAY_HANDLER WHERE  handler_id=#{handlerId}")
    public GatewayHandlerEntity getByHandler(String handlerId);
}
