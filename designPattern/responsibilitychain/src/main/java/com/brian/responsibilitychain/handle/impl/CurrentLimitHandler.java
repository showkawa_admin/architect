package com.brian.responsibilitychain.handle.impl;

import com.brian.responsibilitychain.handle.GatewayHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 接口限流的处理
 */
@Component
@Slf4j
public class CurrentLimitHandler extends GatewayHandler {


    @Override
    public void handle() {
        log.info("网关 - 接口限流处理。。。");
        nextService();
    }
}
