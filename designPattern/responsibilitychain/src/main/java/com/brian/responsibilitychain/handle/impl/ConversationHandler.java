package com.brian.responsibilitychain.handle.impl;

import com.brian.responsibilitychain.handle.GatewayHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 会话处理
 */
@Component
@Slf4j
public class ConversationHandler extends GatewayHandler {

    @Override
    public void handle() {
        log.info("网关 - 会话处理。。。");
        nextService();
    }
}
