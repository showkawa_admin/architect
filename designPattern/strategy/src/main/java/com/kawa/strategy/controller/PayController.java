package com.kawa.strategy.controller;

import com.kawa.strategy.strategy.context.PayContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PayController {

        @Autowired
        private PayContext payContext;
        @GetMapping("/toPay")
        public ResponseEntity<String> toPay(@RequestParam String payCode){

            String result = payContext.toPay(payCode);
            return new ResponseEntity<String>(result,HttpStatus.OK);
        }

        @GetMapping("/")
        public ResponseEntity<String> test(){
            return new ResponseEntity<String>("8080",HttpStatus.OK);
        }
}
