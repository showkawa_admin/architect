package com.kawa.strategy.strategy.impl;

import com.kawa.strategy.strategy.PayStrategy;
import org.springframework.stereotype.Component;

/**
 * 阿里支付
 */
@Component
public class AliPayStrategy implements PayStrategy {
    @Override
    public String toPay() {
        return "调用阿里支付。。。。";
    }
}
