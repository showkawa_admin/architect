package com.kawa.strategy.strategy.context;

import com.kawa.strategy.mapper.PaymentChannelMapper;
import com.kawa.strategy.mapper.entity.PaymentChannelEntity;
import com.kawa.strategy.strategy.PayStrategy;
import com.kawa.strategy.utils.SpringBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class PayContext {

    @Autowired
    PaymentChannelMapper paymentChannelMapper;

    public  String toPay(String payCode){
        PaymentChannelEntity paymentChannel = paymentChannelMapper.getPaymentChannel(payCode);
        if(paymentChannel == null){
            return "没有改配置的支付渠道";
        }
        String beanId = paymentChannel.getStrategyBeanId();
        if(StringUtils.isEmpty(beanId)){
            return "没有改配置的支付渠道beanId";
        }

        PayStrategy payStrategy = SpringBeanUtils.getBean(beanId, PayStrategy.class);
        return payStrategy.toPay();


    }

}
