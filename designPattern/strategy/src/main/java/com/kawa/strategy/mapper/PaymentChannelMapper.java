package com.kawa.strategy.mapper;

import com.kawa.strategy.mapper.entity.PaymentChannelEntity;
import org.apache.ibatis.annotations.Select;

public interface PaymentChannelMapper {
     @Select("SELECT  id as id ,CHANNEL_NAME as CHANNELNAME ,CHANNEL_ID as CHANNELID,STRATEGY_BEAN_ID AS strategybeanid\n" +
             "FROM PAYMENT_CHANNEL where CHANNEL_ID=#{payCode}")
     public PaymentChannelEntity getPaymentChannel(String payCode);
}
