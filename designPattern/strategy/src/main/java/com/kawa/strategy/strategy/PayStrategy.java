package com.kawa.strategy.strategy;

public interface PayStrategy {

    String  toPay();
}
