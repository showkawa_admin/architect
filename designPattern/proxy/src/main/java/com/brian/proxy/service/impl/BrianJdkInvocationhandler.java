package com.brian.proxy.service.impl;

import com.brian.proxy.service.BrianInvocationhandler;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-20 20
 **/
@Slf4j
public class BrianJdkInvocationhandler implements BrianInvocationhandler {

    private Object target;

    public BrianJdkInvocationhandler(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws InvocationTargetException, IllegalAccessException {
        log.info("方法执行前，，，");
        Object result = method.invoke(target, args);//使用Java反射技术执行
        log.info("方法执行后，，，");
        return result;
    }
}
