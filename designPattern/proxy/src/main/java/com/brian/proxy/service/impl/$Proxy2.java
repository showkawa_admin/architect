package com.brian.proxy.service.impl;

import com.brian.proxy.service.BrianInvocationhandler;
import com.brian.proxy.service.OrderService;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @program: architect 模拟动态生成的类
 * @author: Brian Huang
 * @create: 2019-05-20 20
 **/
@Slf4j
public class $Proxy2 implements OrderService {

    protected BrianInvocationhandler h1;

    public $Proxy2(BrianInvocationhandler h1) {
        this.h1 = h1;
    }

    @Override
    public void order() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = OrderService.class.getMethod("order",new Class[]{});
        this.h1.invoke(this,method,null);
    }
}
