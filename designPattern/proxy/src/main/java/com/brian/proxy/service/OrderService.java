package com.brian.proxy.service;

import java.lang.reflect.InvocationTargetException;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-20 20
 **/
public interface OrderService {

    void order() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException;
}
