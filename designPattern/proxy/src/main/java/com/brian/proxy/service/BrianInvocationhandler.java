package com.brian.proxy.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public interface BrianInvocationhandler {
    /**
     * 模拟jdk动态代理 InvocationHandler接口
     * @param proxy 代理对象
     * @param method 被代理类执行的方法
     * @param args 参数
     * @return
     */
   Object invoke(Object proxy, Method method,Object[] args) throws InvocationTargetException, IllegalAccessException;

}
