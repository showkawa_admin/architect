package com.brian.proxy.service.impl;

import com.brian.proxy.service.OrderService;
import lombok.extern.slf4j.Slf4j;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-20 20
 **/
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Override
    public void order() {
        log.info("订单方法执行，，，");
    }
}
