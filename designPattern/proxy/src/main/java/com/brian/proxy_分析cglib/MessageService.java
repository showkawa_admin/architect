package com.brian.proxy_分析cglib;

import java.lang.reflect.InvocationTargetException;

/**
 * @program: base
 * @author: Brian Huang
 * @create: 2020-07-10 21
 **/
public interface MessageService {

    void sendMessage(Object object);
}
