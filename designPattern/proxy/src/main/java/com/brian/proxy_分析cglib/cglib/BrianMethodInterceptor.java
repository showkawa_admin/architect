package com.brian.proxy_分析cglib.cglib;

import lombok.extern.slf4j.Slf4j;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @program: base
 * @author: Brian Huang
 * @create: 2020-07-10 21
 **/
@Slf4j
public class BrianMethodInterceptor implements MethodInterceptor {
    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        log.info("cglib执行前....");
        Object result = methodProxy.invokeSuper(o, objects);
        log.info("cglib执行后....");
        return result;
    }
}
