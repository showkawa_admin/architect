package com.brian.proxy_分析cglib.impl;

import com.brian.proxy_分析cglib.MessageService;
import lombok.extern.slf4j.Slf4j;
import net.sf.cglib.proxy.Callback;

/**
 * @program: base
 * @author: Brian Huang
 * @create: 2020-07-10 21
 **/
@Slf4j
public class MessageServiceImpl implements MessageService, Callback {

    @Override
    public void sendMessage(Object object) {
        log.info("发送航班消息:" + object);
    }
}
