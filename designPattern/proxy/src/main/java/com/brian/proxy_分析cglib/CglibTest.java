package com.brian.proxy_分析cglib;

import com.brian.proxy_分析cglib.cglib.BrianMethodInterceptor;
import com.brian.proxy_分析cglib.impl.MessageServiceImpl;
import com.brian.proxy_分析cglib.impl.MessageServiceImpl$$EnhancerByCGLIB$$dc2beff3;
import net.sf.cglib.core.DebuggingClassWriter;
import net.sf.cglib.proxy.Enhancer;

/**
 * @program: base
 * @author: Brian Huang
 * @create: 2020-07-10 21
 **/
public class CglibTest {

    public static void main(String[] args) {
        System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY,"C:\\Users\\LiangHuang\\Desktop\\work");
//        Enhancer enhancer = new Enhancer();
//        enhancer.setSuperclass(MessageServiceImpl.class);
//        enhancer.setCallback(new BrianMethodInterceptor());
//        MessageServiceImpl messageService = (MessageServiceImpl) enhancer.create();
//        messageService.sendMessage("CX233航班即将起飞...");

        MessageServiceImpl$$EnhancerByCGLIB$$dc2beff3 enhancerByCGLIB$$dc2beff3 = new MessageServiceImpl$$EnhancerByCGLIB$$dc2beff3();
        BrianMethodInterceptor[] brianMethodInterceptors = new BrianMethodInterceptor[1];
        brianMethodInterceptors[0] = new BrianMethodInterceptor();
        enhancerByCGLIB$$dc2beff3.setCallbacks(brianMethodInterceptors);
        enhancerByCGLIB$$dc2beff3.sendMessage("CX880航班30分钟后停靠E22...");



    }
}
