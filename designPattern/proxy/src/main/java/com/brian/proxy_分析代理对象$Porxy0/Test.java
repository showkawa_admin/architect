package com.brian.proxy_分析代理对象$Porxy0;

import com.brian.proxy_分析代理对象$Porxy0.impl.OrderServiceImpl;
import com.brian.proxy_分析代理对象$Porxy0.invocation.JdkInvocationHandler;
import com.sun.corba.se.spi.ior.MakeImmutable;

/**
 * @program: base
 * @author: Brian Huang
 * @create: 2020-07-10 06
 **/
public class Test {


    public static void main(String[] args) {
         // 可以获取动态代理生成的class文件
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");
        JdkInvocationHandler jdkInvocationHandler = new JdkInvocationHandler(new OrderServiceImpl());
        OrderService proxy = jdkInvocationHandler.getProxy();
        proxy.order();
    }

}
