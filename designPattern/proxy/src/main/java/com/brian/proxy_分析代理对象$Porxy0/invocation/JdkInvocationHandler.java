package com.brian.proxy_分析代理对象$Porxy0.invocation;

import com.brian.proxy_分析代理对象$Porxy0.proxy.$Proxy0;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class JdkInvocationHandler implements InvocationHandler {
    /**
     * 目标对象 -- 即被代理对象 OrderServiceImpl
     */
    public Object target;

    public JdkInvocationHandler(Object target) {
        this.target = target;
    }

    // method 接口调用的方法

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println(">>>日志收集开始>>>>");
        // 执行代理对象方法
        Object reuslt = method.invoke(target, args);
        System.out.println(">>>日志收集结束>>>>");
        return reuslt;
    }

    /**
     * 获取代理对象接口
     *
     * @param <T>
     * @return
     */
    public <T> T getProxy() {

//        return (T) Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), this);

        // 使用生成的代理类替换上面的方法
        return (T) new $Proxy0(this);

    }
}
