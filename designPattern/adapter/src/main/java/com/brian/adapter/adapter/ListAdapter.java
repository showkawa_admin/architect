package com.brian.adapter.adapter;

import java.util.HashMap;
import java.util.List;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-28 21
 **/
public class ListAdapter extends HashMap {

    private  List list;

    public ListAdapter(List list) {
        this.list = list;
    }


    @Override
    public  int size() {
        return list.size();
    }

    @Override
    public Object get(Object key) {
        return list.get(Integer.parseInt(key.toString()));
    }
}
