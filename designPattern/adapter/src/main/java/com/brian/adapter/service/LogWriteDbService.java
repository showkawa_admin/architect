package com.brian.adapter.service;

import com.brian.adapter.domain.LogBean;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-06-02 12
 **/
public interface LogWriteDbService {

    public void writeDbFile(LogBean logBean);
}
