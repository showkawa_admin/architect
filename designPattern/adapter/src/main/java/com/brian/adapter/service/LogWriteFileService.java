package com.brian.adapter.service;

import com.brian.adapter.domain.LogBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-06-02 11
 **/
public class LogWriteFileService {
    /**
     * 将日志写入到文件
     */
    public void logWriteFile() {
        System.out.println("将日志写入文件中...");
    }

    /**
     * 从本地文件读取日志
     * @return
     */
    public List<LogBean> readLogFile() {
        LogBean log1 = new LogBean();
        log1.setLogId("10086");
        log1.setLogText("Tomcat启动成功...");

        LogBean log2 = new LogBean();
        log2.setLogId("10087");
        log2.setLogText("Jetty启动成功..。");
        List<LogBean> listArrayList = new ArrayList<>();
        listArrayList.add(log1);
        listArrayList.add(log2);
        return listArrayList;
    }

}
