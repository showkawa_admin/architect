package com.brian.adapter.service.impl;

import com.brian.adapter.domain.LogBean;
import com.brian.adapter.service.LogWriteDbService;
import com.brian.adapter.service.LogWriteFileService;

import java.util.List;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-06-02 12
 **/
public class LogAdapter implements LogWriteDbService {

    private LogWriteFileService logWriteFileService;

    public LogAdapter(LogWriteFileService logWriteFileService) {
        this.logWriteFileService = logWriteFileService;
    }

    @Override
    public void writeDbFile(LogBean logBean) {
        // 1.从文件中读取日志文件
        List<LogBean> logBeans = logWriteFileService.readLogFile();
        // 2.写入到数据库中
        logBeans.add(logBean);
        System.out.println("将数据写入到数据库中...:"+ logBeans.toString());
        // 3.写入到本地文件中
        logWriteFileService.logWriteFile();

    }
}
