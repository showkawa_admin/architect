package com.brian.decorative.service.impl;

import com.brian.decorative.service.BaseGateway;
import com.brian.decorative.service.GatewayDecorate;
import lombok.extern.slf4j.Slf4j;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-14 21:16
 **/
@Slf4j
public class LogComponent  extends GatewayDecorate {

    public LogComponent(BaseGateway baseGateway) {
        super(baseGateway);
    }

    @Override
    public void service() {
        super.service();
        log.info("2-->>> 网关中新增日志收集..");
    }
}
