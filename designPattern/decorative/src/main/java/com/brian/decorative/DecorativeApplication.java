package com.brian.decorative;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DecorativeApplication {

    public static void main(String[] args) {
        SpringApplication.run(DecorativeApplication.class, args);
    }

}
