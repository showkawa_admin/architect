package com.brian.decorative.service;


/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-14 21:13
 **/
public abstract class GatewayDecorate extends BaseGateway {

    private BaseGateway baseGateway;

    public GatewayDecorate(BaseGateway baseGateway) {
        this.baseGateway = baseGateway;
    }

    @Override
    public void service() {
        if(baseGateway !=null){
            baseGateway.service();
        }
    }
}
