package com.brian.decorative.factory;

import com.brian.decorative.service.BaseGateway;
import com.brian.decorative.service.impl.LimitComponent;
import com.brian.decorative.service.impl.LogComponent;
import com.brian.decorative.service.impl.DesensitizationComponent;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-14 21:38
 **/
public class FactoryGateway {

    public static BaseGateway getBaseGateway() {
        return new LimitComponent(new LogComponent(new DesensitizationComponent()));
    }


    public static void main(String[] args) {
        FactoryGateway.getBaseGateway().service();
    }
}
