package com.brian.decorative.service.impl;

import com.brian.decorative.service.BaseGateway;
import lombok.extern.slf4j.Slf4j;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-14 21:15
 **/
@Slf4j
public class DesensitizationComponent  extends BaseGateway {

    @Override
    public void service() {
        log.info("1-->>> 网关中获取基本的操作,数据脱敏...");
    }
}
