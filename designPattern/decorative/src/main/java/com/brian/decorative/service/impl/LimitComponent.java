package com.brian.decorative.service.impl;

import com.brian.decorative.service.BaseGateway;
import com.brian.decorative.service.GatewayDecorate;
import lombok.extern.slf4j.Slf4j;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-14 21:16
 **/
@Slf4j
public class LimitComponent extends GatewayDecorate {

    public LimitComponent(BaseGateway baseGateway) {
        super(baseGateway);
    }

    @Override
    public void service() {
        super.service();
        log.info("3-->>> 网关中新增API接口的限流..");
    }
}
