package com.brian.mongo.annotation;

import java.lang.annotation.*;

/**
 * @program: architect  封装ehcache + redis 的一二级缓存
 * @author: Brian Huang
 * @create: 2019-07-26 07
 **/
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface BrianCache {
}
