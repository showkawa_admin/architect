package com.brian.mongo.annotation;

import java.lang.annotation.*;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-29 19
 **/

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface BrianLock {
}
