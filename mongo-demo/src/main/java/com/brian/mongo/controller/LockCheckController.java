package com.brian.mongo.controller;

import com.brian.mongo.service.RedisLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.redis.util.RedisLockRegistry;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-29 20
 **/

@RestController
public class LockCheckController {

    //springboot2集成的的redis锁的功能
    private RedisLockRegistry redisLockRegistry;

    @Autowired
    private RedisLock  redisLock;



    private Lock lock;

    public LockCheckController(RedisLockRegistry redisLockRegistry) {
        lock = redisLockRegistry.obtain("springboot2-lock");
    }

    @GetMapping("/lockCheck")
    public void test() throws InterruptedException {
        boolean tryLock = lock.tryLock(0, TimeUnit.SECONDS);
        System.out.println("---tryLock---: " + tryLock);
    }

    @GetMapping("/unlock")
    public void testUnlock() throws InterruptedException {
        this.lock.unlock();
    }

    @GetMapping("/lockCheck2")
    public void testL() throws InterruptedException {
        String redisLock = this.redisLock.getRedisLock(5l, 5l);
        System.out.println("---redisLock---: " + redisLock);

    }

    @GetMapping("/unlock02")
    public void testUnlock02(String uuid) throws InterruptedException {
        this.redisLock.unRedisLock(uuid);
    }
}
