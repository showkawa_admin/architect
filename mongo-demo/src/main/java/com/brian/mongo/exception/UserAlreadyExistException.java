package com.brian.mongo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-16 11:09
 **/
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "User already exist!")
public class UserAlreadyExistException  extends  Exception{
}
