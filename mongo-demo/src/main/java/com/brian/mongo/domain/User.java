package com.brian.mongo.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;


/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-16 10:14
 **/
@Data
@Document
public class User implements Serializable {

    @Id
    private String id;
    private String name;
    private String username;
    private String password;

}
