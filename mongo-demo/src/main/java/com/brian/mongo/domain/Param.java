package com.brian.mongo.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-26 07
 **/
public class Param<T> {

    private Map<T, T> map = new HashMap<>();

    private T value;

    public Param() {
    }

    public Param(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        map.put(value,value);
        this.value = value;
    }

    public String getParams() {
        StringBuilder  str =  new StringBuilder();
        Set<Map.Entry<T, T>> sets = map.entrySet();
        for(Map.Entry<T, T> m: sets){
            str.append(m.getValue());
        }
        return str.toString();
    }
}
