package com.brian.hystrix.controller;

import com.brian.hystrix.hystrix.OrderHystrixCommand;
import com.brian.hystrix.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-24 23
 **/
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;


    @GetMapping("/order")
    public ResponseEntity<?> order() throws InterruptedException {
        return new ResponseEntity<>("order", HttpStatus.OK);
    }

    @GetMapping("/orderIndex")
    public ResponseEntity<?> orderIndex() throws InterruptedException {
        return new ResponseEntity<>(new OrderHystrixCommand(orderService).execute(), HttpStatus.OK);
        //return new ResponseEntity<>(orderService.orderIndex(), HttpStatus.OK);
    }
}
