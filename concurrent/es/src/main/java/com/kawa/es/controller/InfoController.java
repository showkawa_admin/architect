package com.kawa.es.controller;

import com.kawa.es.domain.InfoEntity;
import com.kawa.es.repository.InfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-08-12 22
 **/
@RestController
public class InfoController {

    @Autowired
    private InfoRepository infoRepository;

    @GetMapping("/health")
    public ResponseEntity<?> health(){
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    @PostMapping("/szhunagl/addInfo")
    public ResponseEntity<InfoEntity> addInfo(@RequestBody InfoEntity info){
        InfoEntity infoEntity = infoRepository.save(info);
        return new ResponseEntity<>(infoEntity, HttpStatus.OK);
    }

    @GetMapping("/szhunagl/getInfo/{id}")
    public ResponseEntity<InfoEntity> addInfo(@PathVariable(value = "id") String id){
        Optional<InfoEntity> infoEntityOptional = infoRepository.findById(id);
        if(infoEntityOptional.isPresent()){
            return new ResponseEntity<>(infoEntityOptional.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }
}
