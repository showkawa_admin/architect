package com.kawa.es.domain;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-08-12 21
 **/
@Data
public class InfoEntity extends BaseEntity {
    private String name;
    private String post;
    private BigDecimal salary;

}
