package com.kawa.es.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-08-12 21
 **/
@Document(indexName = "szhuangl_es", type = "user")
@Data
public abstract class BaseEntity {

    @Id
    private String id;

}
