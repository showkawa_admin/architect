package com.kawa.es.repository;

import com.kawa.es.domain.CloudDiskEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


public interface CloudDiskRepository extends ElasticsearchRepository<CloudDiskEntity, String> {

}
