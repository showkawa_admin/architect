package com.kawa.shardingjdbc.repository;

import com.kawa.shardingjdbc.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<OrderEntity, Long> {
	@Query(value = "SELECT order_id ,user_id  FROM t_order  where order_id in (?1);", nativeQuery = true)
	public List<OrderEntity> findExpiredOrderState(List<Long> ids);

	@Query(value = "SELECT order_id ,user_id  FROM t_order  where user_id=:userId ", nativeQuery = true)
	public List<OrderEntity> findByUserId(@Param("userId") Long userId);
}