package com.brian.ratelimit.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-25 08
 **/
@Service
public class OrderService {

    public Map order() throws InterruptedException {
        System.out.println(Thread.currentThread().getName() + "---order");
        Map<String, String> map = new HashMap<>();
        map.put("code","200");
        map.put("message","下单成功");
        return map;
    }

    public Map orderIndex() throws InterruptedException {
        Thread.sleep(2000);
        System.out.println(Thread.currentThread().getName() + "---orderIndex");
        Map<String, String> map = new HashMap<>();
        map.put("code","200");
        map.put("message","{'orderId':'QWE21345678'}");
        return map;
    }
}
