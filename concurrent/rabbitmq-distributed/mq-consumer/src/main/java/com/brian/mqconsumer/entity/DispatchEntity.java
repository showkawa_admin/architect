package com.brian.mqconsumer.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-17 20
 **/
@Data
@Entity
@Table(name="platoon")
public class DispatchEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    // 订单号
    private String orderId;
    // 派单路线
    private String dispatchRoute;
    // 外卖员id
    private Long takeoutUserId;
}
