package com.brian.mqproducer.mapper;

import com.brian.mqproducer.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderMapper extends JpaRepository<OrderEntity,Long> {

	public OrderEntity findByOrderId(@Param("order_id") String orderId);



}
