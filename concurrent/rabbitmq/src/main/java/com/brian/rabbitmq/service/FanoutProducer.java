package com.brian.rabbitmq.service;

import com.brian.rabbitmq.domain.MessageEntity;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-11 09
 **/
@Component
//消息生产者投递消息
public class FanoutProducer {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private MessageIdService messageIdService;


    public void sendMessage(String queueName){

        MessageEntity msg = new MessageEntity("许多云在线小工具免费试用", "许多云在线小工具免费试用，网址：xuduocloud.com");
        //请求头中设置消息ID
        String id = messageIdService.putId();
        Message message = MessageBuilder.withBody(msg.toString().getBytes()).setContentType(MessageProperties.CONTENT_TYPE_JSON)
                .setContentEncoding("utf-8").setMessageId(id).build();

        amqpTemplate.convertAndSend(queueName,message);

    }


}
