package com.brian.rabbitmq.service;

import com.brian.rabbitmq.util.SerializeUtil;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * @program: architect 邮件消费者
 * @author: Brian Huang
 * @create: 2019-07-14 22
 **/
@Component
/*
* @RabbitListener 底层使用aop进行拦截，如果程序没有抛出异常，自动提交事务
* 如果程序抛出异常，rabbitmq自动实现补偿机制,将该消息缓存到rabbitmq服务器端，一直到重试成功不抛出异常为止
**/
public class EmailConsumer {

    @Autowired
    private MessageIdService messageIdService;

    @RabbitHandler
    @RabbitListener(queues = "fanout_email_queue")
    public void process(Message message, @Headers Map<String, Object> headers, Channel channel) throws Exception {
        String messageId = message.getMessageProperties().getMessageId();
        String id = messageIdService.getId(messageId);

        if(StringUtils.isEmpty(id)){
            System.out.println("邮件消费者已经消费该消息");
            return;
        }
        String messageEntity = SerializeUtil.Byte2String(message.getBody());
        System.out.println("邮件消费者收到消息：" + messageEntity);
        //模拟调用邮件接口
        double result = Math.random() * 10;
        if(result > 7){
            throw  new Exception("调用第三方邮件接口短信接口失败");
        }

        // 手动ack
        Long deliveryTag = (Long) headers.get(AmqpHeaders.DELIVERY_TAG);
        // 手动签收
        channel.basicAck(deliveryTag, false);
        messageIdService.deleteId(id);
    }

}
