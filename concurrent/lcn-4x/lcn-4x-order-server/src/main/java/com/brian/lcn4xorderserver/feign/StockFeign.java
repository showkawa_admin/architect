package com.brian.lcn4xorderserver.feign;

import com.brian.lcn4xstockserver.service.StockService;
import org.springframework.cloud.openfeign.FeignClient;


@FeignClient("lcn-stock-server")
public interface StockFeign extends StockService {

}
