/**
 * 功能说明:
 * 功能作者:
 * 创建日期:
 * 版权归属:每特教育|蚂蚁课堂所有 www.itmayiedu.com
 */
package com.brian.lcn4xorderserver.service;

import com.brian.lcn4xstockserver.base.ResponseBase;
import org.springframework.web.bind.annotation.GetMapping;


public interface IOrderService {

	/**
	 * 用户下单后调用库存服务进行扣库存
	 * 
	 * @return
	 */
	@GetMapping(value = "/addOrderAndStock")
	public ResponseBase addOrderAndStock(int i) throws Exception;

}
