package com.brian.idempotent.controller;

import com.brian.idempotent.util.TokenUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-28 15
 **/
@RestController
public class TestController {

    @GetMapping("/getToken")
    public String getToken(){
       return TokenUtils.getToken();
    }

    @PostMapping("/test")
    public String test(HttpServletRequest request) {
        String token = request.getHeader("token");
        if(StringUtils.isEmpty(token)){
            return "非法的参数";
        }
        if(!TokenUtils.findToken(token)){
            return "请勿重复提交";
        }

        return "提交成功";
    }
}
