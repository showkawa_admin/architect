package com.brian.springsecurity.service.impl;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-08-05 21
 **/
@Service
public class UserAuthenticationSuccess implements AuthenticationSuccessHandler {


    //用户认证成功
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        httpServletResponse.sendRedirect("/");
    }
}
