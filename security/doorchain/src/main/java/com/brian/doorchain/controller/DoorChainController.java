package com.brian.doorchain.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-05-27 22
 **/
@RestController
public class DoorChainController {


    @GetMapping("/test")
    public String test(@RequestParam String param){
        return param;
    }

}
