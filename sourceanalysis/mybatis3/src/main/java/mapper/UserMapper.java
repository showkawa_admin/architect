package mapper;

import entity.User;


public interface UserMapper {

    public User getUser(int id);
}
