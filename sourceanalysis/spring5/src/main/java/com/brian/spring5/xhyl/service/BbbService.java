package com.brian.spring5.xhyl.service;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-13 20
 **/
@Service
@Scope("prototype")
public class BbbService {

    //@Autowired
    private  AaaService aaaService;

    public void setAaaService(AaaService aaaService) {
        this.aaaService = aaaService;
    }

}
