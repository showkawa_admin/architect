package com.brian.spring5.aop.service;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-09 21
 **/

public class UserService {

    public void showInfo(String name, String age){
        System.out.println("目标方法：name=> " + name +" age=> " + age);
    }
}
