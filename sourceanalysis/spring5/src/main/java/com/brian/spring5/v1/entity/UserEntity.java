package com.brian.spring5.v1.entity;


public class UserEntity {
    private String userName;
    private Integer userId;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public UserEntity(String userName, Integer userId) {
        this.userName = userName;
        this.userId = userId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"userName\":\"")
                .append(userName).append('\"');
        sb.append(",\"userId\":")
                .append(userId);
        sb.append('}');
        return sb.toString();
    }
}
