package com.brian.spring5.aop.invocation;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-09 21
 **/
public interface MethodInvocation {
 public void process();
}
