package com.brian.spring5.v1.test;

import com.brian.spring5.v1.entity.UserEntity;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class V1TestSpring {
    private static ClassPathXmlApplicationContext applicationContext;

    public static void main(String[] args) {
        applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserEntity userEntity = applicationContext.getBean("userEntity", UserEntity.class);
        System.out.println(userEntity.toString());
    }
    /**
     * spring中注入的beanid 如果重复的话，会怎么样？ 启动的时候会报错
     */
}
