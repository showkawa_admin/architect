package com.brian.spring5.xhyl.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-13 20
 **/
@Configuration
@ComponentScan("com.brian.spring5.xhyl.service")
public class MainConfig {
}
