package com.brian.spring5.aop.interceptor;


import com.brian.spring5.aop.invocation.MethodInvocation;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-09 21
 **/
public interface MethodInterceptor {

    public void invoke(MethodInvocation methodInvocation);
}
