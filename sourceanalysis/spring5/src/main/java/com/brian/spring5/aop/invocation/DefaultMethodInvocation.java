package com.brian.spring5.aop.invocation;

import com.brian.spring5.aop.interceptor.MethodInterceptor;
import com.brian.spring5.aop.service.UserService;

import java.util.List;

/**
 * @program: architect
 * @author: Brian Huang
 * @create: 2019-07-09 21
 **/
public class DefaultMethodInvocation implements MethodInvocation{

    private List<MethodInterceptor> methodInterceptors;

    private UserService userService;
    private int currentIndex;

    public DefaultMethodInvocation(List<MethodInterceptor> methodInterceptors, UserService userService) {
        this.methodInterceptors = methodInterceptors;
        this.userService = userService;
    }

    public void process() {
        if(currentIndex == methodInterceptors.size()){
           userService.showInfo("kawa","21");
           return;
        }
        MethodInterceptor methodInterceptor
                = methodInterceptors.get(currentIndex++);
        methodInterceptor.invoke(this);

    }
}
